#pragma once

#include <cstddef>
namespace probe
{
    inline std::size_t linear(const std::size_t hash_code, std::size_t attempt, const std::size_t capacity)
    {
        return (hash_code % capacity + attempt) % capacity;
    }
}
