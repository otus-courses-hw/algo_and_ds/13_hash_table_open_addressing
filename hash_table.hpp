#include <string>
#include <memory>
#include <list>
#include <optional>
#include <functional>

#include "probing.hpp"

template <typename K, typename V, std::size_t Capacity = 1000>
class hash_table
{
    private:
        struct node
        {
            K key;
            V value;
            bool deleted = false;
            bool is_deleted() const {return deleted;}
        };

        using node_ptr = std::unique_ptr<node>;
        using node_array = node_ptr[];
        using array_ptr = std::unique_ptr<node_array>;

        std::size_t capacity;
        std::size_t size;
        array_ptr table;

        void rehashing();
        std::size_t hash_code(const K&) const;
        std::pair<std::size_t, std::optional<std::size_t>> search(const K&) const;

    public:
        hash_table();
        ~hash_table() = default;

        void insert(const K &key, const V value);
        std::optional<V> get(const K &key);
        void remove(const K &key);
};

template <typename K, typename V, std::size_t Capacity>
hash_table<K,V,Capacity>::hash_table() :
    capacity(Capacity),
    size(0),
    table(std::make_unique<node_array>(capacity))
{
}

template <typename K, typename V, std::size_t Capacity>
void hash_table<K,V,Capacity>::insert(const K &key, const V value)
{
    auto [index, deleted_node_index] = search(key);

    if (deleted_node_index.has_value())
        table[deleted_node_index.value()].reset(new node{key, value});
    else
        table[index].reset(new node{key, value});

    if (++size > capacity / 2)
        rehashing();
}

template <typename K, typename V, std::size_t Capacity>
std::optional<V> hash_table<K,V,Capacity>::get(const K &key)
{
    auto [index, deleted_node_index] = search(key);

    if (table[index] == nullptr)
        return std::nullopt;

    if (deleted_node_index.has_value())
    {
        auto prev = deleted_node_index.value();
        table[prev].swap(table[index]);
        return table[prev]->value;
    }
    else
    {
        return table[index]->value;
    }
}

template <typename K, typename V, std::size_t Capacity>
void hash_table<K,V,Capacity>::remove(const K &key)
{
    auto [index, ignored] = search(key);

    if (table[index] != nullptr)
    {
        table[index]->deleted = true;
        --size;
    }
}

template <typename K, typename V, std::size_t Capacity>
void hash_table<K,V,Capacity>::rehashing()
{
    auto new_capacity = capacity * 2;
    auto new_table = std::make_unique<node_array>(new_capacity);
    for (std::size_t i = 0; i < capacity; ++i)
    {
        if (table[i]->is_deleted())
        {
            table[i].reset();
            continue;
        }

        auto hc = hash_code(table[i]->key);
        std::size_t attempt = 0;
        auto index = probe::linear(hc, attempt, capacity);

        while (new_table[index] != nullptr)
        {
            index = probe::linear(hc, ++attempt, capacity);
        }

        new_table[index].swap(table[i]);
    }

    table.reset();
}

template <typename K, typename V, std::size_t Capacity>
std::size_t hash_table<K,V,Capacity>::hash_code(const K &key) const
{
    return std::hash<K>{}(key);
}

template <typename K, typename V, std::size_t Capacity>
std::pair<std::size_t, std::optional<std::size_t>> hash_table<K,V,Capacity>::search(const K &key) const
{
    std::size_t attempt = 0;
    auto hc = hash_code(key);
    std::optional<decltype(attempt)> deleted_node_index;

    std::size_t index = 0;
    while (index < capacity)
    {
        index = probe::linear(hc, attempt, capacity);
        auto &nd = table[index];

        if (nd == nullptr) // a cell is empty
            break;

        if (nd->key == key && !nd->is_deleted()) // the same key exists
            break;

        if (nd->is_deleted() && !deleted_node_index.has_value()) // memorize a firt found deleted node
            deleted_node_index.emplace(index);

        ++attempt;
    }

    return {index, deleted_node_index};
}
