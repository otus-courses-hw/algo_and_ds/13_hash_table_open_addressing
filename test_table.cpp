#include <catch2/catch.hpp>
#include "hash_table.hpp"

TEST_CASE("happy pass", "[hash]")
{
    hash_table<const char*, int> ht;

    ht.insert("stasis", 666);
    CHECK(ht.get("stasis") == 666);

    ht.insert("stasis", 999);
    CHECK(ht.get("stasis") == 999);

    CHECK(ht.get("unexisted_key").has_value() == false);

    ht.remove("stasis");
    CHECK(ht.get("statis") == std::nullopt);
}
